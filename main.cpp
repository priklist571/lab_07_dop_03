//сделать функцию, которая генерирует нормально распределенную псевдослучайную величину с помощью центральной предельной теоремы
//вывести плотность вероятности и распределение
#include <iostream>
#include "Header.h"

using namespace std;

int main()
{
	int mass[RANGE] = { 0 }, i = 0, sum = 0;
	for (int i = 0; i < ITER; i++)
	{
		mass[normal()]++;
	}
	for (int i = 0; i < RANGE; i++)
	{
		cout << "density" << " " << i << " " << (float)mass[i]/(float)ITER << endl;
	}
	cout << endl;
	for (int i = 0; i < RANGE; i++)
	{
		sum = sum + mass[i];
		cout << "distribution" << " " << i << " " << (float)sum / (float)ITER << endl;
	}
	return 0;
}